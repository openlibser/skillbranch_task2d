const express = require('express');
const morgan = require('morgan');

const allowCrossDomain = require('./skillBranchCORS');
const canonizeColor = require('./canonizeColor');

const app = express();

app.use(allowCrossDomain);
app.use(morgan('tiny'));

app.get('/', (req, res) => {
  try {
    const canonnizedColor = canonizeColor(req.query.color);
    res.send(canonnizedColor);
  } catch (err) {
    res.send('Invalid color');
  }
});

app.listen(3000, () => console.log('Server runing on 3000 port!'));
