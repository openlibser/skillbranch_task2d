// expected hue range: [0, 360)
// expected saturation range: [0, 1]
// expected lightness range: [0, 1]
const hslToRgb = (hue, saturation, lightness) => {
  // based on algorithm from http://en.wikipedia.org/wiki/HSL_and_HSV#Converting_to_RGB
  if (hue === undefined) {
    return [0, 0, 0];
  }

  const chroma = (1 - Math.abs((2 * lightness) - 1)) * saturation;
  let huePrime = hue / 60;
  const secondComponent = chroma * (1 - Math.abs((huePrime % 2) - 1));
  huePrime = Math.floor(huePrime);

  let red;
  let green;
  let blue;

  switch (huePrime) {
    case 0:
      red = chroma;
      green = secondComponent;
      blue = 0;
      break;
    case 1:
      red = secondComponent;
      green = chroma;
      blue = 0;
      break;
    case 2:
      red = 0;
      green = chroma;
      blue = secondComponent;
      break;
    case 3:
      red = 0;
      green = secondComponent;
      blue = chroma;
      break;
    case 4:
      red = secondComponent;
      green = 0;
      blue = chroma;
      break;
    case 5:
      red = chroma;
      green = 0;
      blue = secondComponent;
      break;
    default:
      break;
  }

  const lightnessAdjustment = lightness - (chroma / 2);
  red += lightnessAdjustment;
  green += lightnessAdjustment;
  blue += lightnessAdjustment;

  return [Math.round(red * 255), Math.round(green * 255), Math.round(blue * 255)];
};

module.exports = hslToRgb;
