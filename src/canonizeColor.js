const hslToRGB = require('./hslToRGB');

const INVALID_COLOR_ERROR = new Error('Invalid color');

const hexHandler = re => (color) => {
  const hexRGB = color.toLowerCase().match(re).slice(1);

  const sizes = hexRGB.map(col => col.length);
  const sizesSum = sizes.reduce((sum, size) => sum + size, 0);
  const isAllSizesEqual = Math.round(sizesSum / 3) === sizes[0];

  if (!isAllSizesEqual) throw INVALID_COLOR_ERROR;

  return hexRGB.map(colStr => (colStr.length < 2 ? `${colStr}${colStr}` : colStr));
};

const rgbHandler = re => (color) => {
  const decRGB = color.match(re).slice(1).map(colStr => +colStr);

  decRGB.forEach((colComponent) => {
    if (isNaN(colComponent)) throw INVALID_COLOR_ERROR;
    if (colComponent < 0 || colComponent > 255) throw INVALID_COLOR_ERROR;
  });

  const hexRGB = decRGB
    .map(col => col.toString(16))
    .map(colStr => (colStr.length < 2 ? `0${colStr}` : colStr));

  return hexRGB;
};


const hslHandler = re => (color) => {
  const hsl = color.match(re).slice(1)
    .map(comp => +comp)
    .filter(comp => !isNaN(comp));

  if (hsl.length < 3) throw INVALID_COLOR_ERROR;

  const sl = hsl.slice(1);
  sl.forEach((comp) => {
    if (comp < 0 || comp > 100) throw INVALID_COLOR_ERROR;
  });

  const rgbDec = hslToRGB(hsl[0], hsl[1] / 100, hsl[2] / 100);

  const rgbHex = rgbDec
    .map(comp => comp.toString(16))
    .map(colStr => (colStr.length < 2 ? `0${colStr}` : colStr));

  return rgbHex;
};

const colorsRegs = [
  /^#?\s*([\dabcdef]{1,2})([\dabcdef]{1,2})([\dabcdef]{1,2})$/i,
  /^rgb\(\s*(\d{1,3}),\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/i,
  /^hsl\(\s*(\d{1,3})\s*,\s*(\d{1,3})%\s*,\s*(\d{1,3})%\s*\)$/i,
];

const formatsTransforms = {
  [colorsRegs[0]]: hexHandler(colorsRegs[0]),
  [colorsRegs[1]]: rgbHandler(colorsRegs[1]),
  [colorsRegs[2]]: hslHandler(colorsRegs[2]),
};


module.exports = (color) => {
  if (!color) throw INVALID_COLOR_ERROR;

  let clrColor;
  try {
    clrColor = decodeURIComponent(color.trim())
  } catch (e) {
    //ivalid URL encoding test
    clrColor = color.trim().replace(/%\d{2}/ig, '');
  }

  const colorRe = colorsRegs.filter(re => re.test(clrColor))[0];

  if (!colorRe) throw INVALID_COLOR_ERROR;

  const [r, g, b] = formatsTransforms[colorRe](clrColor);

  return `#${r}${g}${b}`;
};
